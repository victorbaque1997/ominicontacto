Release Notes
*************

*Septiembre 15, 2020*

Detalles de Release 1.10.1
==========================

Fixes y mejoras
--------------------------
- Solucionado error que se generaba cuando el agente filtraba grabaciones de llamadas calificadas como gestión
- Solucionada inconsistencia en el reporte de campañas entrantes, ahora todos los datos que se muestran están ligados al rengo de tiempo escogido por el usuario
- Solucionado error en la supervisión de las campañas entrantes si el supervisor no es un administrador
- Solucionado error que sobreescribía los certificados personalizados SSL al realizar una actualización de una instancia
- Se adicionan nuevas opciones al componente RtpEngine
- El componente Asterisk fue configurado para evitar el error "Too much open files"
